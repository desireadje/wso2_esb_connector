package veone.api.beans;

import org.json.JSONObject;

public class AuthFormater extends org.apache.synapse.mediators.AbstractMediator {
	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean mediate(org.apache.synapse.MessageContext context) {
		System.out.println("username :: " + getUsername());
		System.out.println("password :: " + getPassword());

		JSONObject authPayload = new JSONObject();
		JSONObject userJSONObject = new JSONObject();

		// user
		userJSONObject.put("username", getUsername());
		userJSONObject.put("password", getPassword());

		authPayload.put("user", userJSONObject);

		context.setProperty("authPayload", authPayload.toString());

		return true;
	}
}
