package veone.api.beans;

import org.json.JSONObject;
import org.json.XML;

public class JsonValidator extends org.apache.synapse.mediators.AbstractMediator {
    private String jsonBody;

    public String getJsonBody() {
        return jsonBody;
    }

    public void setJsonBody(String jsonBody) {
        this.jsonBody = jsonBody;
    }

    public boolean mediate(org.apache.synapse.MessageContext context) {
        try {
            String soapBody = getJsonBody();
            JSONObject bodyJSONObject = XML.toJSONObject(soapBody).getJSONObject("soapenv:Body");
            JSONObject dataJSONObject = bodyJSONObject.getJSONObject("jsonObject");

            context.setProperty("data", dataJSONObject.toString());

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            System.err.println("Erreur : " + e.getMessage());
        }

        return true;
    }
}
