package veone.api.beans;

public class Main extends org.apache.synapse.mediators.AbstractMediator {
	private String param1;
	private String param2;
	private String param3;

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getParam3() {
		return param3;
	}

	public void setParam3(String param3) {
		this.param3 = param3;
	}

	public boolean mediate(org.apache.synapse.MessageContext context) {
		System.out.println("param1 :: " + getParam1());
		System.out.println("param2 :: " + getParam2());
		System.out.println("param3 :: " + getParam3());

		String result = String.format("Le resultat du retour est : %s %s %s", getParam1(), getParam2(), getParam3());
		System.out.println("result :: " + result);

		context.setProperty("result", result.toString());

		return true;
	}
}